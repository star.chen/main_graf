#!/bin/bash



#read 用來讀取螢幕輸入或是讀取檔案內容。
read -p  "請輸入專案名稱 : " pn
echo "已經輸入專案名稱 : $pn"
read -p "請確認 /opt/process_monitor_ip.list 的名單是否已經更新 (Y/N) ?" clist
if [[ -d "/opt/prom_$pn" ]]; then
   read -p "該專案名稱資料夾已經存在 請確認是否繼續更新 (Y/N) ?" cgo
fi



#先關閉prom_xxx的docker-compose,因為等一下要再開啟
if [[ $cgo == "Y" || $cgo == "" ]]; then
   if [[ $clist == "Y" ]]; then
      echo "請選擇下列要監控的類型 (輸入1 or 2) : "
      echo "1. node基礎監控"
      echo "2. process程序監控"
      read cc12345
      echo "您已經按下$cc12345"
   fi
   if [[ $clist == "Y" ]]; then
      if [[ -d "/opt/prom_$pn" ]]; then
         echo "資料夾存在,先關閉docker-compose"
         cd /opt/prom_$pn
         docker-compose down
      else
         echo "該資料夾不存在,繼續下一步程序..."
      fi
   else
      echo "你已按下N, 請完善 process_monitor_ip.list, 程序即將退出"
   fi
else
   echo "你已按下N, 請確認要更新的專案資料夾, 程序即將退出"
fi



#複製資料夾並且改名
if [[ $cgo == "Y" || $cgo == "" ]]; then
   if [[ $clist == "Y" ]]; then
      echo "你已按下Y, 程序繼續執行..."
      cd /opt/
      if [[ $pn != "mx" ]]; then
         cp ./prom_mx prom_$pn -rf
      fi   
      cd prom_$pn
      if [[ $pn != "mx" ]]; then
         sed -e 's/prometheus_mx/prometheus_'"$pn"'/g' -i docker-compose.yml
         cp -rf /opt/prom_mx/prometheus_mx /opt/prom_$pn/
         mv /opt/prom_$pn/prometheus_mx /opt/prom_$pn/prometheus_$pn
         sed -e 's/mx-monitor/'"$pn"'-monitor/g' -i init_prom.yml
      fi   
      #修改腳本製作設定檔
      yes | cp /opt/process_monitor_ip.list /opt/prom_$pn/process_monitor_ip.list
      cp init_prom.yml prometheus_$pn/prometheus_$pn.yml
      if [[ $cc12345 == "1" ]]; then
         echo "使用gen_nodeexporter_job.sh"
         sh gen_nodeexporter_job.sh prometheus_$pn/prometheus_$pn.yml
      elif [[ $cc12345 == "2" ]]; then
         echo "使用gen_process_exp_job.sh"
         sh gen_process_exp_job.sh prometheus_$pn/prometheus_$pn.yml
      else
         echo "使用gen_nodeexporter_job.sh"
         sh gen_nodeexporter_job.sh prometheus_$pn/prometheus_$pn.yml
      fi
      if [[ $pn != "mx" ]]; then
         cd /opt/prom_$pn/prometheus_$pn
      fi   
      cd /opt/prom_$pn
      docker-compose up -d
   else
      echo "你已按下N, 請完善 process_monitor_ip.list, 程序即將退出"
   fi
else
echo "你已按下N, 請確認要更新的專案資料夾, 程序即將退出"
fi


if [[ -d "/opt/prom_$pn/prom_mx" ]]; then
   yes | rm -rf /opt/prom_$pn/prom_mx
   if [[ -d "/opt/prom_$pn/prometheus_mx" ]]; then
      yes | rm -rf /opt/prom_$pn/prometheus_mx
   fi
   echo "刪除多餘資料夾, 程序結束"
else
   echo "程序結束"
fi
